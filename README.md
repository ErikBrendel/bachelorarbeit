Design and Implementation of a File Format for Representation of Image & Video Processing Pipelines
---------------------------------------------------------------------------------------------------

Bachelor Thesis Erik Brendel

-------------------------------------

[TOC]

-------------------------------------

## Abstract
 - there are lots of effect applying applications
 - image effect format work has been made
 - processing / producing videos brings new challenges
 - this paper proposes a file format for this
 - the evaluation shows that it works out

-------------------------------------
## Introduction

##### Motivation
 - image effects are nice, and wanted by people
 - videos provide more information / more emotion than pictures
 - high potential of temporal and spatial optimization of UGC
 - Why is this even a challenging topic
     - variety of effects due to image/images/video in -> image/images/video out
     - high calculation power / memory requirements
     - different conceptual features needed
         - local metrices (e.g. number of edges in frame)
         - global metrices (e.g. accumulated or relative local metrices)
         - temporal neighbourhood processing (for coherent noise)
     - user interactivity _(should i talk about this?)_

##### Problem Statement
 - "the goal is to support established video effects (skimming / synopsis / chronophoto) in a common file format"
 - gpu accelerated rendering pipelines
     - --> ensure maximum performance
 - platform independent effect specification
     - --> executable on mobile devices, desktop and server systems
 - integrate video with image effect format
     - --> build on established format for frame manipulation
 - stream processing where possible
     - --> to reduce memory usage and imporve user experience
 - _What other problems do I want to tackle?_

##### Contributions
 - _what do I write here?_

##### Structure of this Thesis
 - first explain the thoughts of the xml file
 - showcase possible implementation of pipeline processing application
 - discuss / evaluate / conclude / what is left to do

-------------------------------------
## (Background and) Related Work

##### State of the Art Video Processing
 - blender compositing (just per-frame image based operations)
 - professional video editing software: custom format, not easily re-usable for other input videos, not cross platform
 - _any other references I could use here?_
 
#### Existing paper work
 - prosumer paper by Duerschmidt
 - effect exchange database by soechting

-------------------------------------
## Adaptable File Representation of Video Processing Pipelines

##### Defining asynchronous video processing stages
 - what types of stages exist
     - Image / Video IO
     - Temporal sampling
     - Image Effect
     - Buffered input image effect stage
     - custom cpu stages
 - defining inputs / outputs of stages
     - types like `texture`,`audioSnippet`, `metric:(int|float|bool)`
 - connecting stages
     - connecting input / output slots
     - data flows through the graph

##### Metrices generation and consumption
 - local and neighbourhood metrices easily through stages
 - global metrices as preprocessing step (need to store the video somewhere inbetween)
 - how to express this in the xml files? _(we still need to think about this!)_

-------------------------------------
## Implementation of video effect processing applications
 
##### Runtime Objects
 - class diagram here (for stages / slots / Queues / VideoFrame / ...)
 - each stage is a single thread (to execute in parallel)
 - limited BlockingQueues between two slots
     - --> async parallelity but also limited memoy usage (esp. on mobile devices)

##### Pipeline IO
 - pullparser/builder - based videoPipelineXml-reader component
 - benefits from stage / connection separation in file, because connections are complex and can get read separately

-------------------------------------
## Results and discussion

##### Application examples and Scenarios
 - standalone applications work pretty good for what they were intended
 - thanks to offscreen rendering, service-based video processing is possible
     - even streaming up the video and streamign down the result in parallel is possible for some effects
 - integration into video editing software through plugins possible

##### Implementation Complexity
 - LOC, LOC/Class, cyclomatic Complexity (each on vtk / android video processing pipeline classes)
 - shallow inheritance trees
 - complexity might rise with special stages needed for new effects

##### (Analysis and) Discussion (of Results)
 - based on the variety of effects represented in the given format, it can be considered easy to use and flexible
 - the conceptual structure of async stages allows for fast video processing

-------------------------------------
## Conclusion and Future work

##### Conclusions
 - a flexible video processing framework was presented
 - high performance and cross platform support for complex video effects

##### Future Work
 - prosumer aspects (graphical recombination of video effect segments)
 - cluster computing scalability: Dispatch frame processing to multiple rendering machines (without distorting the effect results) to increase throughput of service-based processing
 - interactive preview of effect parametrization







